package nl.mirabeau.miravault.Application;

import android.app.Application;
import android.content.Context;
import android.nfc.Tag;
import android.util.Log;

/**
 * Created by John van den Berg on 12-5-2015.
 */
public class MiraApplication extends Application{

    private static final String TAG = MiraApplication.class.getSimpleName();

    private static MiraApplication sInstance;

    public static String getVaultAction() {
        return vaultAction;
    }

    public static void setVaultAction(String vaultAction) {
        MiraApplication.vaultAction = vaultAction;
        Log.e(TAG, "vault action set to: "+vaultAction);
    }

    private static String vaultAction;

    private String deviceAction;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }

    public static MiraApplication getsInstance() {
        return sInstance;
    }

    public static Context getAppContext() {
        return sInstance.getApplicationContext();
    }
}
