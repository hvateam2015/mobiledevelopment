package nl.mirabeau.miravault.Application;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import nl.mirabeau.miravault.Model.User;

/**
 * Created by John van den Berg on 14-4-2015.
 */
public class SharedPrefs {
    // LogCat tag
    private static String TAG = SharedPrefs.class.getSimpleName();

    // Shared Preferences
    private static SharedPreferences pref;

    private static SharedPreferences.Editor editor;
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "MirabeauLogin";
    private static final String KEY_INTRO_DONE = "introDone";

    private static final String KEY_EASY_MODE = "easyMode";

    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";

    private static final String KEY_USER_ID = "userId";
    private static final String KEY_USER_NAME = "userName";
    private static final String KEY_USER_EMAIL = "userEmail";

    private static final String KEY_RASPBERRY_IP = "raspberryIP";

    public SharedPrefs(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setLogin(boolean isLoggedIn) {

        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);

        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified!");
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }


    public static void setUser(User user) {
        editor.putString(KEY_USER_ID, user.getUniqueId());
        editor.putString(KEY_USER_NAME, user.getName());
        editor.putString(KEY_USER_EMAIL, user.getEmail());
        editor.commit();
    }

    public static User getUser() {
        User u = new User();
        u.setUniqueId(pref.getString(KEY_USER_ID, "0"));
        u.setName(pref.getString(KEY_USER_NAME, "Uknown User"));
        u.setEmail(pref.getString(KEY_USER_EMAIL, ""));
        return u;
    }

    public void setIntroDone(boolean introDone) {

        editor.putBoolean(KEY_INTRO_DONE, introDone);

        // commit changes
        editor.commit();

        Log.d(TAG, "The app will skip the intro!");

    }

    public boolean introDone(){
        return pref.getBoolean(KEY_INTRO_DONE, false);
    }

    public void setEasyMode(boolean introDone) {

        editor.putBoolean(KEY_EASY_MODE, introDone);

        // commit changes
        editor.commit();
    }

    public boolean getEasyMode(){
        return pref.getBoolean(KEY_EASY_MODE, false);
    }

    public void setIP(String ip) {
        editor.putString(KEY_RASPBERRY_IP, ip);
        editor.commit();
    }

    public String getIP() {
        return pref.getString(KEY_RASPBERRY_IP, "http://192.168.10.36/index.php");
    }
}
