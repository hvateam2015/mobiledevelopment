package nl.mirabeau.miravault.Application;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by John van den Berg on 12-5-2015.
 */
public class DialogHandler {

    private ProgressDialog progressDialog;

    public DialogHandler(Context context) {
        progressDialog = new ProgressDialog(context);
    }

    public void setCanceleable(boolean b) {
        progressDialog.setCancelable(b);
    }

    public void setMessage(String msg) {
        progressDialog.setMessage(msg);
    }

    public void showDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    public void hideDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }
}
