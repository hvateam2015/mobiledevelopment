package nl.mirabeau.miravault.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;

import nl.mirabeau.miravault.Application.MiraApplication;
import nl.mirabeau.miravault.Application.SharedPrefs;
import nl.mirabeau.miravault.R;

/**
 * Created by J van den Berg on 3-6-2015.
 */
public class SettingsDialog extends DialogFragment {

    private EditText ipEditText;
    private Switch easymodeSwitch;

    private SharedPrefs sharedPrefs;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder =  new AlertDialog.Builder(getActivity());
        sharedPrefs = new SharedPrefs(MiraApplication.getAppContext());

        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_settings, null);

        easymodeSwitch = (Switch) view.findViewById(R.id.switch_settings_easymode);
        easymodeSwitch.setChecked(sharedPrefs.getEasyMode());
        ipEditText = (EditText) view.findViewById(R.id.et_settings_ipaddress);
        ipEditText.setText(sharedPrefs.getIP());


        builder.setView(view);

        builder.setPositiveButton("Apply", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                String ip = ipEditText.getText().toString();
                sharedPrefs.setIP(ip);
                boolean easy = easymodeSwitch.isChecked();
                sharedPrefs.setEasyMode(easy);
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                SettingsDialog.this.getDialog().cancel();
            }
        });

        return builder.create();
    }
}
