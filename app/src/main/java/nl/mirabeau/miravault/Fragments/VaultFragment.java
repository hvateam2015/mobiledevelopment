package nl.mirabeau.miravault.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import at.markushi.ui.CircleButton;
import nl.mirabeau.miravault.Application.MiraApplication;
import nl.mirabeau.miravault.Application.SharedPrefs;
import nl.mirabeau.miravault.Beacon.BeaconManager;
import nl.mirabeau.miravault.Beacon.Bluetooth;
import nl.mirabeau.miravault.Network.NetworkData;
import nl.mirabeau.miravault.Network.Requests.VaultRequest;
import nl.mirabeau.miravault.R;

/**
 * Created by John van den Berg on 22-4-2015.
 */
public class VaultFragment extends Fragment {

    public static TextView tv_vault_status, beacontext;

    private static ProgressBar loadingCircle;

    private SharedPrefs sharedPrefs;

    private static CircleButton lockButton;

    private static boolean buttonPressed = false;

    BeaconManager m;

    //TODO Redo the open and close lock display
    //TODO Design the layout
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vault, container, false);

        // setup widgets
        tv_vault_status = (TextView) view.findViewById(R.id.tv_vault_status);
        beacontext = (TextView) view.findViewById(R.id.BeaconText);
        loadingCircle = (ProgressBar) view.findViewById(R.id.loadingCircle);
        lockButton = (CircleButton) view.findViewById(R.id.cb_vault_lock);

        // disable the lock button - enabled when in range of the vault
        DisableLockButton();

        // shared preference
        sharedPrefs = new SharedPrefs(getActivity().getApplicationContext());

        // setup the beacon manager and search for the vault beacon
        m = new BeaconManager(this.getActivity());
        m.beaconFinder();
        Bluetooth bluetooth = new Bluetooth(getActivity());
        bluetooth.checkBluetooth();

        // Set default vault action
        MiraApplication.getsInstance().setVaultAction(NetworkData.LOCK_OPEN);

        lockButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                switch (MiraApplication.getsInstance().getVaultAction()) {
                    case NetworkData.LOCK_OPEN:
                        OpenLock();
                        break;
                    case NetworkData.LOCK_CLOSE:
                        CloseLock();
                        break;
                }
            }
        });
        return view;
    }

    public static void EnableLockButton() {
        beacontext.setText("Beacons found!");
        loadingCircle.setVisibility(View.GONE);
        lockButton.setEnabled(true);
    }

    public static void DisableLockButton() {
        beacontext.setText("Searching for beacons...");
        loadingCircle.setVisibility(View.VISIBLE);
        lockButton.setEnabled(false);
    }

    public void OpenLock() {
        VaultRequest vaultRequest = new VaultRequest(getActivity(), sharedPrefs.getUser(), NetworkData.LOCK_OPEN);
        vaultRequest.execute();
        setButtonPressed(true);

    }

    private void CloseLock() {
        VaultRequest vaultRequest = new VaultRequest(getActivity(), sharedPrefs.getUser(), NetworkData.LOCK_CLOSE);
        vaultRequest.execute();
        setButtonPressed(false);
    }

    /**
     *
     * @return boolean that checks if the button is pressed
     */
    public static boolean isButtonPressed() {
        return buttonPressed;
    }

    /**
     *
     * @param buttonPressed will set the variable once the button is pressed
     */
    public static void setButtonPressed(boolean buttonPressed) {
        VaultFragment.buttonPressed = buttonPressed;
    }
}
