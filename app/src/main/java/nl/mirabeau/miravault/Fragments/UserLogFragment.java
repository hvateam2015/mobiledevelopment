package nl.mirabeau.miravault.Fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import nl.mirabeau.miravault.Adapter.SimpleAdapter;
import nl.mirabeau.miravault.Adapter.SimpleSectionedRecyclerViewAdapter;
import nl.mirabeau.miravault.Decorator.DividerItemDecoration;
import nl.mirabeau.miravault.Network.Requests.LogRequest;
import nl.mirabeau.miravault.R;

/**
 * Created by John van den Berg on 22-4-2015.
 */
public class UserLogFragment extends Fragment {

    private static final String TAG = UserLogFragment.class.getSimpleName();

    private Context context;

    private Spinner spinner_period;
    private ImageButton queryButton;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private Calendar myCalendar;
    private DatePickerDialog.OnDateSetListener date;
    private EditText et_datepicker;

    private List<String> timePeriods;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_log, container, false);

        // get the activity context
        context = getActivity().getApplicationContext();

        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_userlog_log);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));

        // populate the recycler view with log entries
        LogRequest logRequest = new LogRequest(getActivity(), "user");
        logRequest.execute();

        // setup the query button for database queries
        queryButton = (ImageButton) view.findViewById(R.id.btn_userlog_search);
        queryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogRequest logRequest = new LogRequest(getActivity(), "user");
                logRequest.execute();
            }
        });

        // setup the date picker
        myCalendar = Calendar.getInstance();

        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

        et_datepicker = (EditText) view.findViewById(R.id.et_log_date);
        et_datepicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getActivity(), date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        return view;
    }

    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.UK);

        et_datepicker.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
        }
    }
}
