package nl.mirabeau.miravault.Fragments;

/**
 * Created by harispekaric on 12/05/15.
 */
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import nl.mirabeau.miravault.Activity.IntroActivity;
import nl.mirabeau.miravault.Activity.LoginActivity;
import nl.mirabeau.miravault.Application.SharedPrefs;
import nl.mirabeau.miravault.R;

public class ScreenSlidePageFragment3 extends Fragment {

    private Button login;
    private TextView header;
    private TextView subText;
    private SharedPrefs sharedPrefs;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_intro_screen_slide_page3, container, false);

        IntroActivity activity = (IntroActivity) getActivity();

        sharedPrefs = new SharedPrefs(activity.getApplicationContext());

        login = (Button) rootView.findViewById(R.id.login);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                sharedPrefs.setIntroDone(true);
            }
        });

        header = (TextView) rootView.findViewById(R.id.header);
        subText = (TextView) rootView.findViewById(R.id.subText);


        Typeface OpenSansBold = Typeface.createFromAsset(getActivity().getAssets(),"fonts/OpenSans-Bold.ttf");
        header.setTypeface(OpenSansBold);

        Typeface OpenSansLight = Typeface.createFromAsset(getActivity().getAssets(),"fonts/OpenSans-Light.ttf");
        subText.setTypeface(OpenSansLight);
        return rootView;
    }

}