package nl.mirabeau.miravault.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import nl.mirabeau.miravault.Activity.DeviceDetailsActivity;
import nl.mirabeau.miravault.Adapter.DeviceAdapter;
import nl.mirabeau.miravault.Application.SharedPrefs;
import nl.mirabeau.miravault.Model.DeviceItem;
import nl.mirabeau.miravault.Network.Requests.DeviceRequest;
import nl.mirabeau.miravault.R;

/**
 * Created by John van den Berg on 22-4-2015.
 */

public class DeviceLogFragment extends Fragment{

    private ListView deviceListView;
    private SharedPrefs shj;

    private static ArrayList<DeviceItem> devices = new ArrayList<>();
    private DeviceAdapter deviceAdapter;
    private ArrayAdapter<CharSequence> spinnerAdapter;
    private FloatingActionButton fab;

    private Spinner deviceSpinner;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Get the view from fragmenttab1.xml
        View view = inflater.inflate(R.layout.fragment_device_log, container, false);
        deviceListView = (ListView) view.findViewById(R.id.deviceList);

        //Set the views
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.activity_main_swipe_refresh_layout);
        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.attachToListView(deviceListView);

        //set the listeners
        fab.setOnClickListener(addDeviceHandler);
        deviceListView.setOnItemClickListener(deviceItemHandler);
        mSwipeRefreshLayout.setOnRefreshListener(refreshHandler);

        //Send a fetch request
        DeviceRequest deviceRequest = new DeviceRequest(getActivity(), "fetch");
        deviceRequest.execute();

        //Inflate the layout of the spinner
        View header = getActivity().getLayoutInflater().inflate(R.layout.spinner, null);
        deviceSpinner = (Spinner) header.findViewById(R.id.deviceSpinner);
        spinnerAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.deviceSpinnerItems, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choicesspinnerr
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        deviceSpinner.setAdapter(spinnerAdapter);
        //Add the spinner as a header for the listview
        deviceListView.addHeaderView(header);

        //Spinner onitem selected listener
        deviceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                devices = DeviceRequest.getDevices();
                deviceAdapter = new DeviceAdapter(getActivity(), R.layout.row_device_item, devices);
                deviceListView.setAdapter(deviceAdapter);
                switch (position) {
                    case 0:
                        deviceAdapter.sort(new Comparator<DeviceItem>() {
                            @Override
                            public int compare(DeviceItem device1, DeviceItem device2) {
                                String a = device1.getBorrowed();
                                String b = device2.getBorrowed();
                                int borrowComp = a.compareTo(b);

                                if (borrowComp != 0) {
                                    return borrowComp;
                                } else {
                                    String c = device1.getUsername();
                                    String d = device2.getUsername();
                                    return c.compareToIgnoreCase(d);
                                }
                            }
                        });
                        break;
                    case 1:
                        deviceAdapter.sort(new Comparator<DeviceItem>() {
                            @Override
                            public int compare(DeviceItem device1, DeviceItem device2) {
                                return device1.getDeviceBrand().compareToIgnoreCase(device2.getDeviceBrand());
                            }
                        });
                        break;
                    case 2:
                        deviceAdapter.sort(new Comparator<DeviceItem>() {
                            @Override
                            public int compare(DeviceItem device1, DeviceItem device2) {
                                return device1.getDeviceModel().compareToIgnoreCase(device2.getDeviceModel());
                            }
                        });
                        break;
                    case 3:
                        deviceAdapter.sort(new Comparator<DeviceItem>() {
                            @Override
                            public int compare(DeviceItem device1, DeviceItem device2) {
                                return device1.getDeviceOS().compareToIgnoreCase(device2.getDeviceOS());
                            }
                        });
                        break;
                    default:
                        break;

                }
                deviceAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }

    //Listener for the item clicklistener
    ListView.OnItemClickListener deviceItemHandler = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            //Get the value of the item that the user clicked on
            DeviceItem clickedItem = (DeviceItem) parent.getItemAtPosition(position);

            //Create the intent
            Intent intent = new Intent(DeviceLogFragment.this.getActivity(), DeviceDetailsActivity.class);
            String deviceId = Integer.toString(clickedItem.getDeviceId());
            intent.putExtra("deviceID", deviceId);
            intent.putExtra("deviceBrand", clickedItem.getDeviceBrand());
            intent.putExtra("deviceModel", clickedItem.getDeviceModel());
            intent.putExtra("deviceOS", clickedItem.getDeviceOS());
            intent.putExtra("deviceBorrowed", clickedItem.getBorrowed());
            intent.putExtra("name", clickedItem.getUsername());
            intent.putExtra("uniqueID", clickedItem.getUnique_id());

            //Open the new screen by starting the activity
            startActivity(intent);
        }
    };

    //Listener for the add button
    View.OnClickListener addDeviceHandler = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(DeviceLogFragment.this.getActivity());
            LinearLayout layout = new LinearLayout(DeviceLogFragment.this.getActivity());
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setPadding(50, 50, 50, 50);

            builder.setTitle("Add device");

            //initialize the views for the alertdialog
            final EditText brandText = new EditText(DeviceLogFragment.this.getActivity());
            brandText.setHint("Brand");
            brandText.setPadding(10, 50, 10, 50);
            layout.addView(brandText);

            final EditText modelText = new EditText(DeviceLogFragment.this.getActivity());
            modelText.setHint("Model");
            modelText.setPadding(10, 50, 10, 50);
            layout.addView(modelText);

            final EditText osText = new EditText(DeviceLogFragment.this.getActivity());
            osText.setHint("Operating system");
            osText.setPadding(10, 50, 10, 50);
            layout.addView(osText);

            builder.setView(layout);

            builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (TextUtils.isEmpty(brandText.getText().toString()) || TextUtils.isEmpty(modelText.getText().toString()) || TextUtils.isEmpty(osText.getText().toString())) {
                        Toast.makeText(DeviceLogFragment.this.getActivity(), "Values are empty!", Toast.LENGTH_LONG).show();
                    } else {
                        DeviceRequest deviceRequest = new DeviceRequest(getActivity(), "add", brandText.getText().toString(), modelText.getText().toString(), osText.getText().toString());
                        deviceRequest.execute();
                        devices = DeviceRequest.getDevices();
                        Log.d("Devices after adding", devices.toString());
                        deviceAdapter.notifyDataSetChanged();
                    }
                }
            });
            builder.setNegativeButton("Cancel", null);
            builder.show();
        }
    };

    //Onrefresh listener
    SwipeRefreshLayout.OnRefreshListener refreshHandler = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            DeviceRequest deviceRequest = new DeviceRequest(getActivity(), "fetch");
            deviceRequest.execute();

            deviceListView.setAdapter(deviceAdapter);
            deviceAdapter.notifyDataSetChanged();

            Toast.makeText(DeviceLogFragment.this.getActivity(), "Refreshed", Toast.LENGTH_LONG).show();
            mSwipeRefreshLayout.setRefreshing(false);
        }
    };

}