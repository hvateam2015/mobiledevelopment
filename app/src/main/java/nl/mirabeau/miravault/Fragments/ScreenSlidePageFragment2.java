package nl.mirabeau.miravault.Fragments;

/**
 * Created by harispekaric on 12/05/15.
 */
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import nl.mirabeau.miravault.R;

public class ScreenSlidePageFragment2 extends Fragment {

    private TextView header;
    private TextView subText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_intro_screen_slide_page2, container, false);

        header = (TextView) rootView.findViewById(R.id.header);
        subText = (TextView) rootView.findViewById(R.id.subText);


        Typeface OpenSansBold = Typeface.createFromAsset(getActivity().getAssets(),"fonts/OpenSans-Bold.ttf");
        header.setTypeface(OpenSansBold);

        Typeface OpenSansLight = Typeface.createFromAsset(getActivity().getAssets(),"fonts/OpenSans-Light.ttf");
        subText.setTypeface(OpenSansLight);
        return rootView;
    }
}
