package nl.mirabeau.miravault.Beacon;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;

/**
 * Created by Fatih C on 4/22/2015.
 */
public class Bluetooth {

    Activity activity;

    private final static int REQUEST_ENABLE_BT = 1;

    public Bluetooth(Activity activity){
        this.activity = activity;
    }

    public void checkBluetooth(){

        //initiate bluetooth constructor (object)
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        //check if there is a bluetooth adapter
        if(bluetoothAdapter == null){
            //Toast.makeText(getApplicationContext(), "This device does not support bluetooth", Toast.LENGTH_LONG).show();
        }

        //check if it's enabled
        if(!bluetoothAdapter.isEnabled()){
            //if it's not. Enable it.
            Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.startActivityForResult(enableBluetooth, REQUEST_ENABLE_BT);
        }
    }
}
