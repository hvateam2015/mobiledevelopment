package nl.mirabeau.miravault.Beacon;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.Region;

import java.util.List;

import nl.mirabeau.miravault.Activity.DeviceDetailsActivity;
import nl.mirabeau.miravault.Activity.MainActivity;
import nl.mirabeau.miravault.Application.SharedPrefs;
import nl.mirabeau.miravault.Fragments.VaultFragment;
import nl.mirabeau.miravault.Network.NetworkData;
import nl.mirabeau.miravault.Network.Requests.DeviceRequest;
import nl.mirabeau.miravault.Network.Requests.VaultRequest;

/**
 * Created by Fatih C on 4/22/2015.
 */
public class BeaconManager{

    Activity activity;
    private SharedPrefs sharedPrefs;

    // Tags
    private static final String TAG = BeaconManager.class.getSimpleName();

    // Estimote
    private static com.estimote.sdk.BeaconManager beaconManager;
    private final static int REQUEST_ENABLE_BT = 1;
    private static final String ESTIMOTE_PROXIMITY_UUID = "B9407F30-F5F8-466E-AFF9-25556B57FE6D";
    private static final Region ALL_ESTIMOTE_BEACONS = new Region("regionId", ESTIMOTE_PROXIMITY_UUID, null, null);
    private Button openLock;

    private boolean open = false;

    public BeaconManager(Activity activity) {
        this.activity = activity;
    }

    public void beaconFinder(){

        sharedPrefs = new SharedPrefs(activity);


        //initiate beaconManager object with context as argument
       beaconManager = new com.estimote.sdk.BeaconManager(activity);

        //consistent range listener. Scans near beacons consistently and stores them in List<Beacon> array (ArrayList)
        beaconManager.setRangingListener(new com.estimote.sdk.BeaconManager.RangingListener() {
            @Override public void onBeaconsDiscovered(Region region, List<Beacon> beacons) {

                //if it found a beacon, adjust a textView in the layout.
                //tv_beacons.setText("Found beacon(s) - \n");
                //tv_inrange.setText("");
                //display information within the textView
                for(int i = 0; i < beacons.size(); i++){
                    if(beacons.get(i).getMinor() == 1994){
                        if(calculateAccuracy(beacons.get(i).getMeasuredPower(), beacons.get(i).getRssi()) < 0.5){
                            if(sharedPrefs.getEasyMode() && open == false){
                                VaultRequest vaultRequest = new VaultRequest(activity, sharedPrefs.getUser(), NetworkData.LOCK_OPEN);
                                vaultRequest.execute();
                                open = true;
                            } else {
                                VaultFragment.EnableLockButton();
                            }
                        }else{
                            if(sharedPrefs.getEasyMode() && open == true){
                                VaultRequest vaultRequest = new VaultRequest(activity, sharedPrefs.getUser(), NetworkData.LOCK_CLOSE);
                                vaultRequest.execute();
                                open = false;
                            } else {
                                VaultFragment.DisableLockButton();
                            }
                            if(VaultFragment.isButtonPressed()){
                                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                                stopSearching();
                                builder.setMessage("Did you borrow a device?");

                                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                builder.setNegativeButton("No", null);
                                builder.show();
                            }
                        }
                    }
                }
            }
        });

        //connecting to the beacons through the startRanging methods with region as argument.
        beaconManager.connect(new com.estimote.sdk.BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                try {
                    beaconManager.startRanging(ALL_ESTIMOTE_BEACONS);
                } catch (RemoteException e) {
                    Log.e("", "Cannot start ranging", e);
                }
            }
        });
    }

    //calculate distance
    protected static double calculateAccuracy(int txPower, double rssi) {
        if (rssi == 0) {
            // if we cannot determine accuracy, return -1.
            return -1.0;
        }

        double ratio = rssi*1.0/txPower;
        if (ratio < 1.0) {
            return Math.pow(ratio,10);
        }
        else {
            double accuracy =  (0.89976)*Math.pow(ratio,7.7095) + 0.111;
            return (double) Math.round(accuracy * 1000) / 1000;
        }
    }

    public static void stopSearching() {
        beaconManager.disconnect();
    }

}
