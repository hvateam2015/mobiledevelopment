package nl.mirabeau.miravault.Network.Requests;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import nl.mirabeau.miravault.Activity.MainActivity;
import nl.mirabeau.miravault.Adapter.DeviceAdapter;
import nl.mirabeau.miravault.Application.DialogHandler;
import nl.mirabeau.miravault.Application.SharedPrefs;
import nl.mirabeau.miravault.Fragments.DeviceLogFragment;
import nl.mirabeau.miravault.Model.DeviceItem;
import nl.mirabeau.miravault.Model.JSONError;
import nl.mirabeau.miravault.Network.NetworkData;
import nl.mirabeau.miravault.Network.Parser.JSONParser;
import nl.mirabeau.miravault.Network.VolleyHandler;
import nl.mirabeau.miravault.R;

/**
 * Created by JDH on 12-05-15.
 */
public class DeviceRequest{

    private static final String TAG = DeviceRequest.class.getSimpleName();

    private DialogHandler dialogHandler;
    private SharedPrefs sharedPrefs;

    private VolleyHandler volleyHandler;
    private RequestQueue requestQueue;

    private String deviceAction;
    public JSONArray jsonArray;
    private Activity activity;

    private String deviceId;
    private String brand;
    private String model;
    private String os;
    private String uniqueID;
    private String borrowed;

    private static ArrayList<DeviceItem> devices = new ArrayList<>();

    private ListView deviceList;
    private DeviceAdapter deviceAdapter;

    //Constructor to fetch all devices
    public DeviceRequest(Activity activity, String deviceAction) {
        this.activity = activity;
        this.deviceAction = deviceAction;

        // progress dialog
        dialogHandler = new DialogHandler(activity);
        dialogHandler.setCanceleable(false);

        // shared preference
        sharedPrefs = new SharedPrefs(activity.getApplicationContext());

        // volley
        volleyHandler = VolleyHandler.getInstance();
        requestQueue = volleyHandler.getRequestQueue();
    }

    //Constructor to add device
    public DeviceRequest(Activity activity, String deviceAction, String brand, String model, String os ) {
        this.activity = activity;
        this.deviceAction = deviceAction;
        this.brand = brand;
        this.model = model;
        this.os = os;

        // progress dialog
        dialogHandler = new DialogHandler(activity);
        dialogHandler.setCanceleable(false);

        // shared preference
        sharedPrefs = new SharedPrefs(activity.getApplicationContext());

        // volley
        volleyHandler = VolleyHandler.getInstance();
        requestQueue = volleyHandler.getRequestQueue();
    }

    //Constructor to update all devices
    public DeviceRequest(Activity activity, String deviceAction, String deviceId,
                         String brand, String model, String os, String borrowed, String uniqueID) {
        this.activity = activity;
        this.deviceAction = deviceAction;
        this.deviceId = deviceId;
        this.brand = brand;
        this.model = model;
        this.os = os;
        this.borrowed = borrowed;
        this.uniqueID = uniqueID;

        // progress dialog
        dialogHandler = new DialogHandler(activity);
        dialogHandler.setCanceleable(false);

        // shared preference
        sharedPrefs = new SharedPrefs(activity.getApplicationContext());

        // volley
        volleyHandler = VolleyHandler.getInstance();
        requestQueue = volleyHandler.getRequestQueue();
    }

    //Constructor to delete all devices
    public DeviceRequest(Activity activity, String deviceAction, String deviceId) {
        this.activity = activity;
        this.deviceAction = deviceAction;
        this.deviceId = deviceId;

        // progress dialog
        dialogHandler = new DialogHandler(activity);
        dialogHandler.setCanceleable(false);

        // shared preference
        sharedPrefs = new SharedPrefs(activity.getApplicationContext());

        // volley
        volleyHandler = VolleyHandler.getInstance();
        requestQueue = volleyHandler.getRequestQueue();
    }

    public void execute() {
        dialogHandler.setMessage("Collect devices...");
        dialogHandler.showDialog();

        StringRequest deviceRequest = new StringRequest(Request.Method.POST, NetworkData.URL_DEVICES_REQUEST, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.d(TAG, "Device Response: " + s.toString());
                dialogHandler.hideDialog();
                try {

                    // Turn the response string into a JSON object
                    JSONObject jObj = new JSONObject(s);
                    // Initialize the JSON parser
                    JSONParser parser = new JSONParser();
                    // Parse response
                    JSONError jsonError = parser.ParseError(jObj);

                    boolean error = jsonError.isErrorValue();

                    // Check for error node in json
                    if (!error) {
//                        if(deviceAction == "fetch") {

                            jsonArray = jObj.getJSONArray("devices");
                            devices = parser.parseDevices(jsonArray);
                            setDevices(devices);
//                            devices.addAll(parser.parseDevices(jsonArray));
//                        } else {
//                            DeviceItem device = parser.ParseDevice(jObj);
//                            devices = getDevices();
//                            if(deviceAction == "add" || deviceAction == "update"){
//                                devices.add(device);
//                            }
//                                devices.remove(device);
//                            }
                        //}
                        deviceList = (ListView) activity.findViewById(R.id.deviceList);
                        deviceAdapter = new DeviceAdapter(activity, R.layout.row_device_item, devices);
                        deviceList.setAdapter(deviceAdapter);
                        deviceAdapter.notifyDataSetChanged();
                    }

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "Vault Error: " + volleyError.getMessage());
                Toast.makeText(activity, volleyError.getMessage(), Toast.LENGTH_LONG).show();
                dialogHandler.hideDialog();
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                switch(deviceAction){
                    case "fetch":
                        params.put("tag", deviceAction);
                        break;
                    case "add":
                        params.put("tag", deviceAction);
                        params.put("brand", brand);
                        params.put("model", model);
                        params.put("os", os);
                        break;
                    case "update":
                        params.put("tag", deviceAction);
                        params.put("deviceID", deviceId);
                        params.put("brand", brand);
                        params.put("model", model);
                        params.put("os", os);
                        params.put("borrowed", borrowed);
                        params.put("uniqueID", uniqueID);
                        break;
                    case "delete":
                        params.put("tag", deviceAction);
                        params.put("deviceID", deviceId);
                        break;
                    default:
                        break;
                }
                return params;
            }
        };
        requestQueue.add(deviceRequest);

    }

    public static ArrayList<DeviceItem> getDevices() {
        return devices;
    }

    public static void setDevices(ArrayList<DeviceItem> devices) {
        DeviceRequest.devices = devices;
    }
}
