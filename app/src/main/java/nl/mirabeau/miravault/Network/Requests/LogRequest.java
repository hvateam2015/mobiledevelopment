package nl.mirabeau.miravault.Network.Requests;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.mirabeau.miravault.Adapter.SimpleAdapter;
import nl.mirabeau.miravault.Adapter.SimpleSectionedRecyclerViewAdapter;
import nl.mirabeau.miravault.Model.LogItem;
import nl.mirabeau.miravault.Application.DialogHandler;
import nl.mirabeau.miravault.Application.SharedPrefs;
import nl.mirabeau.miravault.Model.JSONError;
import nl.mirabeau.miravault.Network.NetworkData;
import nl.mirabeau.miravault.Network.Parser.JSONParser;
import nl.mirabeau.miravault.Network.VolleyHandler;
import nl.mirabeau.miravault.R;

/**
 * Created by John van den Berg on 13-5-2015.
 */
public class LogRequest {

    private static final String TAG = LogRequest.class.getSimpleName();

    private DialogHandler dialogHandler;
    private SharedPrefs sharedPrefs;

    private VolleyHandler volleyHandler;
    private RequestQueue requestQueue;

    private Activity activity;
    private String logType;

    public JSONArray jsonArray;

    public LogRequest(Activity activity, String logType) {
        this.activity = activity;
        this.logType = logType;

        // progress dialog
        dialogHandler = new DialogHandler(activity);
        dialogHandler.setCanceleable(false);

        // shared preference
        sharedPrefs = new SharedPrefs(activity.getApplicationContext());

        // volley
        volleyHandler = VolleyHandler.getInstance();
        requestQueue = volleyHandler.getRequestQueue();
    }

    public JSONArray execute() {
        dialogHandler.setMessage("Retrieving log...");
        dialogHandler.showDialog();

        StringRequest loginReq = new StringRequest(Request.Method.POST, NetworkData.URL_LOG, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.d(TAG, "Log Response: " + s.toString());
                dialogHandler.hideDialog();
                try {
                    // Turn the response string into a JSON object
                    JSONObject jObj = new JSONObject(s);
                    // Initialize the JSON parser
                    JSONParser parser = new JSONParser();
                    // Parse response
                    JSONError jsonError = parser.ParseError(jObj);
                    boolean error = jsonError.isErrorValue();

                    // Check for error node in json
                    if (!error) {
                        jsonArray = jObj.getJSONArray("log");
                        ArrayList<LogItem> logEntries = parser.ParseLogEntries(jsonArray);
                        ArrayList<String> dates = new ArrayList<>();
                        ArrayList<Integer> dateIndex = new ArrayList<>();
                        dates.add(logEntries.get(0).getDate());
                        dateIndex.add(0);
                        String lastDate = dates.get(0);

                        for (int i=0; i<logEntries.size(); i++) {
                            if (!lastDate.equals(logEntries.get(i).getDate())) {
                                dates.add(logEntries.get(i).getDate());
                                dateIndex.add(i);
                                lastDate = logEntries.get(i).getDate();
                            }
                        }

                        RecyclerView logList = (RecyclerView) activity.findViewById(R.id.rv_userlog_log);

                        //Your RecyclerView.Adapter
                        SimpleAdapter mAdapter = new SimpleAdapter(activity, logEntries);

                        //This is the code to provide a sectioned list
                        List<SimpleSectionedRecyclerViewAdapter.Section> sections = new ArrayList<>();

                        //Sections
                        for (int i=0; i<dates.size(); i++) {
                            sections.add( new SimpleSectionedRecyclerViewAdapter.Section(dateIndex.get(i), dates.get(i)));
                        }

                        //Add your adapter to the sectionAdapter
                        SimpleSectionedRecyclerViewAdapter.Section[] dummy = new SimpleSectionedRecyclerViewAdapter.Section[sections.size()];
                        SimpleSectionedRecyclerViewAdapter mSectionedAdapter = new SimpleSectionedRecyclerViewAdapter(activity, R.layout.row_userlog_section, R.id.section_text, mAdapter);
                        mSectionedAdapter.setSections(sections.toArray(dummy));

                        //Apply this adapter to the RecyclerView
                        logList.setAdapter(mSectionedAdapter);

                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jsonError.getErrorMessage();
                        Toast.makeText(activity, errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "Log Error: " + volleyError.getMessage());
                Toast.makeText(activity, volleyError.getMessage(), Toast.LENGTH_LONG).show();
                dialogHandler.hideDialog();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<>();
                params.put("tag", logType);

                return params;
            }
        };
        requestQueue.add(loginReq);
        return null;
    }
}
