package nl.mirabeau.miravault.Network.Requests;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import nl.mirabeau.miravault.Activity.MainActivity;
import nl.mirabeau.miravault.Application.DialogHandler;
import nl.mirabeau.miravault.Application.SharedPrefs;
import nl.mirabeau.miravault.Model.JSONError;
import nl.mirabeau.miravault.Model.User;
import nl.mirabeau.miravault.Network.NetworkData;
import nl.mirabeau.miravault.Network.Parser.JSONParser;
import nl.mirabeau.miravault.Network.VolleyHandler;

/**
 * Created by John van den Berg on 6-5-2015.
 */
public class LoginRequest {

    private static final String TAG = LoginRequest.class.getSimpleName();

    private DialogHandler dialogHandler;
    private SharedPrefs sharedPrefs;

    private VolleyHandler volleyHandler;
    private RequestQueue requestQueue;

    private Activity activity;
    private String email, password;

    public LoginRequest(Activity activity, String email, String password) {
        this.activity = activity;
        this.email = email;
        this.password = password;


        // progress dialog
        dialogHandler = new DialogHandler(activity);
        dialogHandler.setCanceleable(false);

        // shared preference
        sharedPrefs = new SharedPrefs(activity.getApplicationContext());

        // volley
        volleyHandler = VolleyHandler.getInstance();
        requestQueue = volleyHandler.getRequestQueue();
    }

    public void execute() {
        dialogHandler.setMessage("Logging in...");
        dialogHandler.showDialog();

        StringRequest loginReq = new StringRequest(Request.Method.POST, NetworkData.URL_LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.d(TAG, "Login Response: " + s.toString());
                dialogHandler.hideDialog();
                try {
                    // Turn the response string into a JSON object
                    JSONObject jObj = new JSONObject(s);
                    // Initialize the JSON parser
                    JSONParser parser = new JSONParser();
                    // Parse response
                    JSONError jsonError = parser.ParseError(jObj);
                    User user = parser.ParseUser(jObj);

                    boolean error = jsonError.isErrorValue();

                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        // Create login session
                        sharedPrefs.setLogin(true);
                        // Set user
                        sharedPrefs.setUser(user);

                        // Launch main activity
                        Intent intent = new Intent(activity, MainActivity.class);
                        activity.startActivity(intent);
                        activity.finish();
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jsonError.getErrorMessage();
                        Toast.makeText(activity, errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "Login Error: " + volleyError.getMessage());
                Toast.makeText(activity, volleyError.getMessage(), Toast.LENGTH_LONG).show();
                dialogHandler.hideDialog();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);

                return params;
            }
        };
        requestQueue.add(loginReq);
    }
}
