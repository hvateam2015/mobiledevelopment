package nl.mirabeau.miravault.Network.Requests;

import android.app.Activity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import nl.mirabeau.miravault.Application.DialogHandler;
import nl.mirabeau.miravault.Application.MiraApplication;
import nl.mirabeau.miravault.Application.SharedPrefs;
import nl.mirabeau.miravault.Model.JSONError;
import nl.mirabeau.miravault.Model.User;
import nl.mirabeau.miravault.Network.NetworkData;
import nl.mirabeau.miravault.Network.Parser.JSONParser;
import nl.mirabeau.miravault.Network.VolleyHandler;
import nl.mirabeau.miravault.R;

/**
 * Created by John van den Berg on 12-5-2015.
 */
public class VaultRequest {

    private static final String TAG = VaultRequest.class.getSimpleName();

    private DialogHandler dialogHandler;
    private SharedPrefs sharedPrefs;

    private VolleyHandler volleyHandler;
    private RequestQueue requestQueue;

    private Activity activity;
    private User user;
    private String action;
    private TextView tv_lockstatus;
    private String raspberryIP;

    public VaultRequest(Activity activity, User user, String action) {
        this.activity = activity;
        this.user = user;
        this.action = action;

        // progress dialog
        dialogHandler = new DialogHandler(activity);
        dialogHandler.setCanceleable(false);

        // shared preference
        sharedPrefs = new SharedPrefs(activity.getApplicationContext());
        raspberryIP = sharedPrefs.getIP();
        // volley
        volleyHandler = VolleyHandler.getInstance();
        requestQueue = volleyHandler.getRequestQueue();

        // widget
        tv_lockstatus = (TextView) activity.findViewById(R.id.tv_vault_status);
    }

    public void execute() {
        dialogHandler.setMessage("Contacting lock...");
        dialogHandler.showDialog();


        StringRequest vaultRequest = new StringRequest(Request.Method.POST, raspberryIP, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.e(TAG, "Vault Response: " + s.toString());
                dialogHandler.hideDialog();
                try {
                    // Turn the response string into a JSON object
                    JSONObject jObj = new JSONObject(s);
                    // Initialize the JSON parser
                    JSONParser parser = new JSONParser();
                    // Parse response
                    JSONError jsonError = parser.ParseError(jObj);

                    boolean error = jsonError.isErrorValue();

                    if (!error) {
                        JSONObject lockObject = jObj.getJSONObject("lock");
                        String lockStatus = lockObject.getString("lock_status");

                        // Set the vault action
                        switch (lockStatus) {
                            case NetworkData.LOCK_OPEN:
                                MiraApplication.setVaultAction(NetworkData.LOCK_CLOSE);
                                tv_lockstatus.setText("Lock is "+lockStatus);
                                break;
                            case NetworkData.LOCK_CLOSE:
                                MiraApplication.setVaultAction(NetworkData.LOCK_OPEN);
                                tv_lockstatus.setText("Lock is "+lockStatus+"d");
                                break;
                        }

                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jsonError.getErrorMessage();
                        Toast.makeText(activity, errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "Vault Error: " + volleyError.getMessage());
                Toast.makeText(activity, volleyError.getMessage(), Toast.LENGTH_LONG).show();
                dialogHandler.hideDialog();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<>();
                params.put("tag", action);
                params.put("name", user.getName());
                params.put("email", user.getEmail());

                return params;
            }
        };
        requestQueue.add(vaultRequest);
    }
}