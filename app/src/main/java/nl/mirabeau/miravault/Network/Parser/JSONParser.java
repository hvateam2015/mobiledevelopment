package nl.mirabeau.miravault.Network.Parser;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import nl.mirabeau.miravault.Model.DeviceItem;
import nl.mirabeau.miravault.Model.LogItem;
import nl.mirabeau.miravault.Model.JSONError;
import nl.mirabeau.miravault.Model.User;
import nl.mirabeau.miravault.R;

/**
 * Created by John van den Berg on 12-5-2015.
 */
public class JSONParser {

    private static final String TAG = JSONParser.class.getSimpleName();

    public JSONError ParseError(JSONObject jsonObject) {
        JSONError error = new JSONError();
        try {
            JSONObject errorObject = jsonObject.getJSONObject("error");
            error.setErrorValue(errorObject.getBoolean("error_value"));
            error.setErrorMessage(errorObject.getString("error_msg"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return error;
    }

    public User ParseUser(JSONObject jsonObject) {
        User user = new User();
        try {
            JSONObject userObject = jsonObject.getJSONObject("user");
            user.setUniqueId(userObject.getString("unique_id"));
            user.setName(userObject.getString("name"));
            user.setEmail(userObject.getString("email"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return user;
    }

    public ArrayList<LogItem> ParseLogEntries(JSONArray jsonArray) {
        if (jsonArray != null) {
            ArrayList<LogItem> arrayList = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    LogItem logItem = new LogItem();
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    // fetch data from json
                    String datetime = jsonObject.getString("datetime");
                    String[] parts = datetime.split(" ");

                    // construct time
                    String time = parts[1];
                    time = time.substring(0, 5);

                    // construct date
                    String date = parts[0];
                    String[] dateParts = date.split("-");
                    date = dateParts[2]+"-"+dateParts[1]+"-"+dateParts[0];

                    String name = jsonObject.getString("username");
                    String action = jsonObject.getString("action");

                    // set log item
                    logItem.setTime(time);
                    logItem.setDate(date);
                    logItem.setName(name);
                    logItem.setAction(action);

                    arrayList.add(logItem);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return arrayList;
        } else {
            return null;
        }
    }

    public ArrayList<DeviceItem> parseDevices(JSONArray JSONArray) {
        if (JSONArray != null) {
            ArrayList<DeviceItem> devices = new ArrayList<>();
            for (int i = 0; i < JSONArray.length(); i++) {
                try {
                    DeviceItem deviceItem = new DeviceItem();
                    JSONObject jsonObject = JSONArray.getJSONObject(i);

                    int deviceId = jsonObject.getInt("deviceID");
                    String deviceBrand = jsonObject.getString("brand");
                    String deviceModel = jsonObject.getString("model");
                    String deviceOS = jsonObject.getString("os");
                    String deviceBorrowed = jsonObject.getString("borrowed");
                    String username = jsonObject.getString("name");
                    String userUniqueID = jsonObject.getString("uniqueID");

                    deviceItem.setDeviceId(deviceId);
                    deviceItem.setDeviceBrand(deviceBrand);
                    deviceItem.setDeviceModel(deviceModel);
                    deviceItem.setDeviceOS(deviceOS);
                    deviceItem.setBorrowed(deviceBorrowed);
                    deviceItem.setUsername(username);
                    deviceItem.setUnique_id(userUniqueID);

                    deviceItem.setImage(R.mipmap.ic_launcher);

                    devices.add(deviceItem);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return devices;

        } else {
            return null;
        }
    }

    public DeviceItem ParseDevice(JSONObject jsonObject) {
        DeviceItem deviceItem = new DeviceItem();
        try {
            JSONObject deviceObject = jsonObject.getJSONObject("device");
            int deviceId = deviceObject.getInt("deviceID");
            String deviceBrand = deviceObject.getString("brand");
            String deviceModel = deviceObject.getString("model");
            String deviceOS = deviceObject.getString("os");
            String deviceBorrowed = deviceObject.getString("borrowed");

            deviceItem.setDeviceId(deviceId);
            deviceItem.setDeviceBrand(deviceBrand);
            deviceItem.setDeviceModel(deviceModel);
            deviceItem.setDeviceOS(deviceOS);
            deviceItem.setBorrowed(deviceBorrowed);
            deviceItem.setImage(R.mipmap.ic_launcher);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return deviceItem;
    }


}
