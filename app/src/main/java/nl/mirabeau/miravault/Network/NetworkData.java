package nl.mirabeau.miravault.Network;

import nl.mirabeau.miravault.Application.SharedPrefs;

/**
 * Created by John van den Berg on 14-4-2015.
 */
public class NetworkData {

    // Server user login url
//    public static String URL_LOGIN = "http://digi-trainer.com/login.php";
    public static String URL_LOGIN = "http://oege.ie.hva.nl/~hullijj001/Mirabeau/login.php";
    // Server log request url
//    public static String URL_LOG = "http://digi-trainer.com/login.php";
    public static String URL_LOG = "http://oege.ie.hva.nl/~hullijj001/Mirabeau/log.php";
    // Server devices request url
//    public static String URL_DEVICES_REQUEST = "http://digi-trainer.com/login.php";
    public static String URL_DEVICES_REQUEST = "http://oege.ie.hva.nl/~hullijj001/Mirabeau/devices.php";

    // Open / close lock request
    public final static String LOCK_OPEN = "open";
    public final static String LOCK_CLOSE = "close";

}
