package nl.mirabeau.miravault.Model;

/**
 * Created by John van den Berg on 12-5-2015.
 */
public class JSONError {
    private boolean errorValue;
    private String errorMessage;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isErrorValue() {
        return errorValue;
    }

    public void setErrorValue(boolean errorValue) {
        this.errorValue = errorValue;
    }
}
