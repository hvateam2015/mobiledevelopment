package nl.mirabeau.miravault.Model;

import java.io.Serializable;

/**
 * Created by JDH on 22-04-15.
 */
public class DeviceItem implements Comparable{

    private int deviceId;
    private String deviceBrand;
    private String deviceModel;
    private String deviceOS;
    private int image;
    private String username;
    private String borrowed;
    private String unique_id;

    public DeviceItem() {
    }

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceBrand() {
        return deviceBrand;
    }

    public void setDeviceBrand(String deviceBrand) {
        this.deviceBrand = deviceBrand;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getDeviceOS() {
        return deviceOS;
    }

    public void setDeviceOS(String deviceOS) {
        this.deviceOS = deviceOS;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public String getBorrowed() {
        return borrowed;
    }

    public void setBorrowed(String borrowed) {
        this.borrowed = borrowed;
    }

    @Override
    public String toString(){
        return "ID: " + deviceId + ", devicebrand: " + deviceBrand +
                ", devicemodel: " + deviceModel + ", device operating system: " +
                deviceOS + ", borrowed: " + borrowed + ", by who: " + username + ", unique_id: " + unique_id;
    }

    @Override
    public int compareTo(Object another) {
        return 0;
    }
}
