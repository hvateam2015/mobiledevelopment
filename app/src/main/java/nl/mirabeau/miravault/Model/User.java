package nl.mirabeau.miravault.Model;

/**
 * Created by JDH on 22-04-15.
 */
public class User {

    private String uniqueId;
    private String name;
    private String email;

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
