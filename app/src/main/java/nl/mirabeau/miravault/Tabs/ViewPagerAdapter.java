package nl.mirabeau.miravault.Tabs;

/**
 * Created by John van den Berg on 22-4-2015.
 */
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import nl.mirabeau.miravault.Fragments.DeviceLogFragment;
import nl.mirabeau.miravault.Fragments.UserLogFragment;
import nl.mirabeau.miravault.Fragments.VaultFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 3;
    // Tab Titles
    private String tabtitles[] = new String[] { "Vault", "User Log", "Devices" };
    Context context;

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {

            // Open FragmentTab1.java
            case 0:
                VaultFragment fragmentVault = new VaultFragment();
                return fragmentVault;

            // Open FragmentTab2.java
            case 1:
                UserLogFragment fragmentUserLog = new UserLogFragment();
                return fragmentUserLog;

            // Open FragmentTab3.java
            case 2:
                DeviceLogFragment fragmentDeviceLog = new DeviceLogFragment();
                return fragmentDeviceLog;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabtitles[position];
    }
}