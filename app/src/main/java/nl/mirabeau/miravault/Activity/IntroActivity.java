package nl.mirabeau.miravault.Activity;

/**
 * Created by harispekaric on 12/05/15.
 */
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;

import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

import nl.mirabeau.miravault.Application.SharedPrefs;
import nl.mirabeau.miravault.Fragments.ScreenSlidePageFragment1;
import nl.mirabeau.miravault.Fragments.ScreenSlidePageFragment2;
import nl.mirabeau.miravault.Fragments.ScreenSlidePageFragment3;
import nl.mirabeau.miravault.R;


public class IntroActivity extends FragmentActivity {

    private static final int NUM_PAGES = 3;

    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private PageIndicator mIndicator;

    // Session manager
    private SharedPrefs session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_screen_slide);

        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);

        mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
        mIndicator.setViewPager(mPager);

        // reference session manager
        session = new SharedPrefs(getApplicationContext());

        // check if the user has already seen the intro
        if (session.introDone()) {
            // user has already seen the intro
            Intent intent = new Intent(IntroActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }

    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment1 objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch(position){
                case 0:
                    ScreenSlidePageFragment1 s = new ScreenSlidePageFragment1();
                    return s;
                case 1:
                    ScreenSlidePageFragment2 f = new ScreenSlidePageFragment2();
                    return f;
                case 2:
                    ScreenSlidePageFragment3 a = new ScreenSlidePageFragment3();
                    return a;
            }
            return null;
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

}