package nl.mirabeau.miravault.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import nl.mirabeau.miravault.Network.NetworkData;
import nl.mirabeau.miravault.Network.Requests.LoginRequest;
import nl.mirabeau.miravault.R;
import nl.mirabeau.miravault.Application.SharedPrefs;


public class LoginActivity extends Activity implements OnClickListener{

    // Tags
    private static final String TAG = LoginActivity.class.getSimpleName();

    // Session manager
    private SharedPrefs session;

    // Widgets
    private Button btn_login_login;
    private EditText et_login_email, et_login_password;

    // Progress Dialog
    private ProgressDialog pDialog;

    // Database
    private NetworkData dbConfig = new NetworkData();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // reference session manager
        session = new SharedPrefs(getApplicationContext());

        // check if the user is already logged in
        if (session.isLoggedIn()) {
            // user is already logged
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            finish();
            startActivity(intent);
        }

        // progress dialog
        pDialog = new ProgressDialog(this);

        // reference widgets
        btn_login_login = (Button) findViewById(R.id.btn_login_login);
        et_login_email = (EditText) findViewById(R.id.et_login_email);
        et_login_password = (EditText) findViewById(R.id.et_login_password);

        // setup listeners
        btn_login_login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login_login :
                if (isLoginValid(et_login_email, et_login_password)) {
                    LoginRequest loginRequest = new LoginRequest(LoginActivity.this, et_login_email.getText().toString(), et_login_password.getText().toString());
                    loginRequest.execute();
                }
                break;
        }
    }

    private boolean isLoginValid(EditText email, EditText password) {

        String _email = email.getText().toString();
        String _password = password.getText().toString();

        // check for empty fields
        if (TextUtils.isEmpty(_email) || TextUtils.isEmpty(_password)) {
            Toast.makeText(getApplicationContext(), "Please enter both fields", Toast.LENGTH_LONG).show();
            return false;
        }

        // check for valid email and password
        if (isEmailValid(_email) && isPasswordValid(_password)) {
            return true;
        }
        return false;
    }

    private boolean isEmailValid(String email) {
        if (!email.contains("@") || !email.contains(".")) {
            Toast.makeText(getApplicationContext(), "Invalid email", Toast.LENGTH_LONG).show();
        }
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        if (password.length() <= 4) {
            Toast.makeText(getApplicationContext(), "Invalid password", Toast.LENGTH_LONG).show();
        }
        return password.length() > 4;
    }
}
