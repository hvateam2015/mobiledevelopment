package nl.mirabeau.miravault.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import nl.mirabeau.miravault.Application.SharedPrefs;
import nl.mirabeau.miravault.Beacon.BeaconManager;
import nl.mirabeau.miravault.Model.DeviceItem;
import nl.mirabeau.miravault.Network.Requests.DeviceRequest;
import nl.mirabeau.miravault.R;


public class DeviceDetailsActivity extends ActionBarActivity {

    private TextView deviceBorrowed, lastBorrowedTextView;
    private static EditText deviceBrand, deviceModel, deviceOS;
    private static Button deleteBtn, updateBtn;
    private static Switch borrowSwitch;

    private String deviceId, brandText, modelText, osText, name, borrowed, uniqueID;

    private SharedPrefs sharedPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_details);

        //set the views
        deviceBrand = (EditText) findViewById(R.id.deviceBrand);
        deviceModel = (EditText) findViewById(R.id.deviceModel);
        deviceOS = (EditText) findViewById(R.id.deviceOS);
        deleteBtn = (Button) findViewById(R.id.deleteBtn);
        updateBtn = (Button) findViewById(R.id.updateBtn);
        deviceBorrowed = (TextView) findViewById(R.id.borrowdTextView);
        borrowSwitch = (Switch) findViewById(R.id.switch_borrow);
        lastBorrowedTextView = (TextView) findViewById(R.id.lastBorrowedTextView);

        //get the data from the intent
        deviceId = getIntent().getStringExtra("deviceID");
        brandText = getIntent().getStringExtra("deviceBrand");
        modelText = getIntent().getStringExtra("deviceModel");
        osText = getIntent().getStringExtra("deviceOS");
        borrowed = getIntent().getStringExtra("deviceBorrowed");
        name = getIntent().getStringExtra("name");
        uniqueID = getIntent().getStringExtra("uniqueID");

        //Set the listeners
        deleteBtn.setOnClickListener(deleteListener);
        updateBtn.setOnClickListener(updateListener);

        deviceBrand.setText(brandText);
        deviceModel.setText(modelText);
        deviceOS.setText(osText);

        String lastBorrowed;
        //If the device is available
        if(borrowed.equals("false")){
            //and if the retrieved name is equal to null, set the string to none
            if(name.equals("null")){
                lastBorrowed = "none";
            } else {
                //else set the string to retrieved name
                lastBorrowed = name;
            }
            lastBorrowedTextView.setText("Last borrowed by: " + lastBorrowed);
            deviceBorrowed.setText("Device currently available");
            //If device is borrowed
        } else {
            //If device is borrowed by the current user
            if(sharedPrefs.getUser().getUniqueId().equals(uniqueID)){
                deviceBorrowed.setText("Device currently borrowed by you");
                borrowSwitch.setChecked(true);
                //if device is borrowed by someone else
            } else {
                deviceBorrowed.setText("Device currently borrowed by: " + name);
                borrowSwitch.setClickable(false);
            }
        }

    }

    View.OnClickListener deleteListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (borrowed.equals("false") || sharedPrefs.getUser().getUniqueId().equals(uniqueID)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(DeviceDetailsActivity.this);
                builder.setMessage("Are you sure?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Send request to delete the device
                        DeviceRequest deviceRequest = new DeviceRequest(DeviceDetailsActivity.this, "delete", deviceId);
                        deviceRequest.execute();
                    }
                });
                builder.setNegativeButton("No", null);
                builder.show();
            } else {
                Toast.makeText(DeviceDetailsActivity.this, "Device is currently borrowed", Toast.LENGTH_LONG).show();
            }
        }
    };

    View.OnClickListener updateListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(borrowSwitch.isChecked()){
                borrowed = "true";
                uniqueID = sharedPrefs.getUser().getUniqueId();
            } else {
                if(!uniqueID.equals(sharedPrefs.getUser().getUniqueId()) && borrowed.equals("true")){
                    borrowed = "true";
                } else {
                    borrowed = "false";
                }
            }
            //Send request to update the device
            DeviceRequest deviceRequest = new DeviceRequest(DeviceDetailsActivity.this, "update",
                    deviceId, deviceBrand.getText().toString(), deviceModel.getText().toString(),
                    deviceOS.getText().toString(), borrowed, uniqueID);
            deviceRequest.execute();
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_device_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
