package nl.mirabeau.miravault.Adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import nl.mirabeau.miravault.Model.LogItem;
import nl.mirabeau.miravault.R;

/**
 * Created by J van den Berg on 2-6-2015.
 */
public class SimpleAdapter extends RecyclerView.Adapter<SimpleAdapter.SimpleViewHolder> {

    private final Context mContext;
    private List<LogItem> mData;

    public void add(LogItem s,int position) {
        position = position == -1 ? getItemCount()  : position;
        mData.add(position,s);
        notifyItemInserted(position);
    }

    public void remove(int position){
        if (position < getItemCount()  ) {
            mData.remove(position);
            notifyItemRemoved(position);
        }
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        public final TextView title;
        public final TextView time;
        public final ImageView icon;

        public SimpleViewHolder(View view) {
            super(view);
            time = (TextView) view.findViewById(R.id.tv_userlog_time);
            title = (TextView) view.findViewById(R.id.tv_userlog_username);
            icon = (ImageView) view.findViewById(R.id.iv_userlog_icon);
        }
    }

    public SimpleAdapter(Context context, ArrayList<LogItem> data) {
        mContext = context;
        if (data != null)
            mData = data;
        else mData = new ArrayList<>();
    }

    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.row_userlog_entry, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, final int position) {
        holder.time.setText(mData.get(position).getTime());
        holder.title.setText(mData.get(position).getName());

        if (mData.get(position).getAction().equals("open")) {
            holder.icon.setImageResource(R.drawable.ic_lock_open);
        } else {
            holder.icon.setImageResource(R.drawable.ic_lock_closed);
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}