package nl.mirabeau.miravault.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import nl.mirabeau.miravault.Application.SharedPrefs;
import nl.mirabeau.miravault.Model.DeviceItem;
import nl.mirabeau.miravault.R;

/**
 * Created by JDH on 19-05-15.
 */
public class DeviceAdapter extends ArrayAdapter<DeviceItem> {

    private boolean selectable = false;

    private SharedPrefs sharedPref;

    private LayoutInflater inflater;

    public DeviceAdapter(Context context, int resource, List<DeviceItem> devices){
        super(context, resource, devices);

        inflater = LayoutInflater.from(getContext());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View row = convertView;
        ViewHolder holder;

        //check if the row is new
        if(row == null){
            //inflate the layout if it didn' exist yet
            row = inflater.inflate(R.layout.row_device_item, parent, false);

            //create a new view holder instance
            holder = new ViewHolder(row);

            //set the holder as a tag so we can get it back later
            row.setTag(holder);

        } else {
            //the row isn't new so we can reuse the viewholder
            holder = (ViewHolder) row.getTag();
        }
        //populate the row
        holder.populateRow(getItem(position));

        return row;
    }

    public void setSelectable(boolean selectable){
        this.selectable = selectable;
        notifyDataSetChanged();
    }

    public boolean isSelectable()
    {
        return selectable;
    }

    class ViewHolder{
        private ImageView image;
        private TextView brand, model, os, status;
        private CheckBox checkBox;

        public ViewHolder(View view){
            image = (ImageView) view.findViewById(R.id.image);
            brand = (TextView) view.findViewById(R.id.deviceBrand);
            model = (TextView) view.findViewById(R.id.deviceModel);
            os = (TextView) view.findViewById(R.id.deviceOS);
            checkBox = (CheckBox) view.findViewById(R.id.checkBox);
            status = (TextView) view.findViewById(R.id.deviceStatus);
        }

        //Method to set the values in a row
        public void populateRow(DeviceItem device){

            //set the views for this row
            image.setImageResource((device.getImage()));
            if(selectable == false){
                checkBox.setVisibility(View.GONE);
            } else {
                checkBox.setVisibility(View.VISIBLE);
            }
            if (device.getBorrowed().equals("false")){
                status.setText("Status: available");
            } else {
                if(device.getUnique_id().equals(sharedPref.getUser().getUniqueId())){
                    status.setText("Status: borrowed by you");
                } else {
                    status.setText("Status: borrowed by " + device.getUsername());
                }
            }

            brand.setText(device.getDeviceBrand());
            model.setText("Model: " + device.getDeviceModel());
            os.setText("OS: " + device.getDeviceOS());



        }
    }


}
