-- phpMyAdmin SQL Dump
-- version 4.4.6.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Gegenereerd op: 22 jun 2015 om 12:52
-- Serverversie: 5.6.25
-- PHP-versie: 5.3.29-pl0-gentoo

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `zhullijj001`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `devices`
--

CREATE TABLE IF NOT EXISTS `devices` (
  `deviceID` int(11) NOT NULL,
  `brand` varchar(40) NOT NULL,
  `model` varchar(40) NOT NULL,
  `os` varchar(40) NOT NULL,
  `borrowed` varchar(10) NOT NULL,
  `uniqueID` varchar(23) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `devices`
--

INSERT INTO `devices` (`deviceID`, `brand`, `model`, `os`, `borrowed`, `uniqueID`) VALUES
(2, 'Nokia', 'Lumia', 'Windows Phone', 'true', '5566e85525e6f3.30305334'),
(3, 'Apple', '6Plus', '9.0.0', 'false', '5565885d4bfba5.80886289'),
(7, 'One Plus', 'One', 'Lollipop', 'true', '5565a4b2791187.84251890'),
(39, 'Apple', '6', '8.0.4', 'true', '5565a4b2791187.84251890'),
(40, 'Samsung', 'S5', 'Kitkat', 'true', '5565885d4bfba5.80886289'),
(41, 'LG', 'Nexus 5', 'Lollipop', 'true', '5565885d4bfba5.80886289'),
(42, 'Motorola', 'Nexus 6', 'Lollipop', 'true', '5565885d4bfba5.80886289'),
(43, 'Haris', 'Pekaric', 'HP', 'false', NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `logid` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `datetime` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=302 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `log`
--

INSERT INTO `log` (`logid`, `username`, `action`, `datetime`) VALUES
(36, 'Haris Pekaric', 'open', '2015-05-27 09:48:08'),
(37, 'Haris Pekaric', 'closed', '2015-05-27 09:48:10'),
(184, 'Haris Pekaric', 'open', '2015-06-02 10:54:11'),
(185, 'Haris Pekaric', 'closed', '2015-06-02 10:54:12'),
(273, 'John van den Berg', 'closed', '2015-06-01 10:00:00'),
(274, 'John van den Berg', 'open', '2015-06-01 09:59:00'),
(275, 'john van den berg', 'open', '2015-06-03 09:58:02'),
(276, 'john van den berg', 'closed', '2015-06-03 09:58:11'),
(277, 'john van den berg', 'open', '2015-06-03 09:58:17'),
(278, 'john van den berg', 'closed', '2015-06-03 09:58:18'),
(279, 'john van den berg', 'open', '2015-06-03 09:58:20'),
(280, 'john van den berg', 'closed', '2015-06-03 09:58:27'),
(281, 'john van den berg', 'open', '2015-06-03 09:58:28'),
(282, 'john van den berg', 'closed', '2015-06-03 09:58:29'),
(283, 'john van den berg', 'open', '2015-06-03 09:58:30'),
(284, 'john van den berg', 'closed', '2015-06-03 09:58:31'),
(285, 'john van den berg', 'open', '2015-06-03 09:58:32'),
(286, 'john van den berg', 'closed', '2015-06-03 09:58:33'),
(287, 'john van den berg', 'open', '2015-06-03 09:58:34'),
(288, 'john van den berg', 'closed', '2015-06-03 09:58:36'),
(289, 'john van den berg', 'open', '2015-06-03 11:05:26'),
(290, 'john van den berg', 'closed', '2015-06-03 11:05:31'),
(291, 'joshua', 'open', '2015-06-03 11:06:31'),
(292, 'joshua', 'open', '2015-06-03 11:21:06'),
(293, 'joshua', 'closed', '2015-06-03 11:21:09'),
(294, 'joshua', 'open', '2015-06-03 11:21:12'),
(295, 'joshua', 'closed', '2015-06-03 11:21:15'),
(296, 'joshua', 'open', '2015-06-03 11:21:17'),
(297, 'joshua', 'open', '2015-06-03 11:59:17'),
(298, 'joshua', 'open', '2015-06-03 11:59:51'),
(299, 'john van den berg', 'open', '2015-06-03 12:25:56'),
(300, 'john van den berg', 'open', '2015-06-03 12:46:23'),
(301, 'john van den berg', 'closed', '2015-06-03 12:46:32');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(11) NOT NULL,
  `unique_id` varchar(23) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `encrypted_password` varchar(80) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`uid`, `unique_id`, `name`, `email`, `encrypted_password`, `salt`, `created_at`, `updated_at`) VALUES
(1, '5565885d4bfba5.80886289', 'john van den berg', 'john44berg@hotmail.com', 'TckIqd2qtFrMsUihOU2iv+bb0xo5OWQ1N2RiNDE0', '99d57db414', '2015-05-27 09:03:25', NULL),
(2, '5565a4b2791187.84251890', 'Haris', 'haris@hotmail.com', '9UfEZCP8lhdL6bKphHkO2CKUSsE0ZWRlNDY1Mjcy', '4ede465272', '2015-05-27 11:04:18', NULL),
(3, '5566e85525e6f3.30305334', 'jdh', 'jdh@hotmail.com', 'XaOtYTiSCxd68uRg6f5zU/DTVVlkYzRlMzFmYzUw', 'dc4e31fc50', '2015-05-28 10:05:09', NULL),
(4, '556826ea59ea19.16378463', 'joshua', 'joshua@hotmail.com', 'OyH0O0K5ngIQCLqVMFW8Vayy+fdhOTVhMjdhNzc1', 'a95a27a775', '2015-05-29 08:44:26', NULL);

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`deviceID`),
  ADD KEY `INDEX_UNIQUE_ID_DEVICES` (`uniqueID`);

--
-- Indexen voor tabel `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`logid`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`uid`),
  ADD KEY `INDEX_UNIQUE_ID` (`unique_id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `devices`
--
ALTER TABLE `devices`
  MODIFY `deviceID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT voor een tabel `log`
--
ALTER TABLE `log`
  MODIFY `logid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=302;
--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Beperkingen voor geëxporteerde tabellen
--

--
-- Beperkingen voor tabel `devices`
--
ALTER TABLE `devices`
  ADD CONSTRAINT `fk_uniqueID` FOREIGN KEY (`uniqueID`) REFERENCES `users` (`unique_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
